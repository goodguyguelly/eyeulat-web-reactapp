import React, { Component } from "react";
import axios from "axios";

class Home extends Component {
  state = {
    loading: true,
    success: "false",
    users: []
  };

  componentDidMount() {
    let appName = "EyeULAT";
    document.title = "Home | " + appName;
    axios
      .get("/api/v1/users")
      .then(result => {
        this.setState({ loading: false });
        this.setState({ success: "true" });
        this.setState({ users: result.data.users });
      })
      .catch(err => {
        this.setState({ loading: false });
        this.setState({ success: "false" });
      });
  }

  render() {
    const listUsers =
      this.state.loading === true ? (
        <div key="0">Loading... Please Wait.</div>
      ) : this.state.success === "true" ? (
        this.state.users.map(user => (
          <div key={user.id}>
            <b>Name: </b>
            {user.name} <br />
            <b>Username:</b> {user.username}
            <hr />
          </div>
        ))
      ) : (
        <div key="0">No Data</div>
      );
    return <div>{listUsers}</div>;
  }
}

export default Home;
