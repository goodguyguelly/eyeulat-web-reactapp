import React, { Component } from "react";

class Error extends Component {
  render() {
    return (
      <div>
        <p>ERROR 404! Page not found.</p>
      </div>
    );
  }
}

export default Error;
